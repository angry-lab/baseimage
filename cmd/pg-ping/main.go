package main

import (
	"database/sql"
	"log"
	"os"
	"time"

	_ "github.com/lib/pq"

	"github.com/cenkalti/backoff/v4"
	"github.com/spf13/pflag"
)

var (
	dsn     = pflag.StringP("dsn", "d", "", "postgres URL")
	timeout = pflag.DurationP("timeout", "t", 30*time.Second, "timeout")
	verbose = pflag.BoolP("verbose", "v", false, "verbose errors")
)

func main() {
	pflag.Parse()

	go func() {
		<-time.After(*timeout)
		log.Println("exit by timeout")
		os.Exit(1)
	}()

	db, err := sql.Open("postgres", *dsn)
	if err != nil {
		log.Fatalf("cannot open connection: %s", err)
	}

	err = backoff.Retry(func() error {
		err := db.Ping()
		if *verbose && err != nil {
			log.Printf("ping: %s", err)
		}

		return err
	}, backoff.NewExponentialBackOff())
	if err != nil {
		log.Fatalf("backoff error: %s", err)
	}

	if *verbose {
		log.Println("success!")
	}
}
