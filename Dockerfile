FROM golang:1.15-alpine as builder

RUN apk add --no-cache git

COPY . /src
WORKDIR /src

ENV CGO_ENABLED 0
ENV GO111MODULE on

RUN go build -v -o /pg-ping ./cmd/pg-ping/main.go
RUN go build -v -o /mysql-ping ./cmd/mysql-ping/main.go


FROM docker:dind

COPY --from=builder /pg-ping /usr/local/bin/pg-ping
COPY --from=builder /mysql-ping /usr/local/bin/mysql-ping

ENV K8S_VERSION="v1.19.3"
ENV DOCTL_VERSION="1.53.0"
ENV MIGRATE_VERSION="v4.13.0"

RUN apk add --no-cache ca-certificates bash git openssh-client make curl \
  && curl -L https://github.com/golang-migrate/migrate/releases/download/${MIGRATE_VERSION}/migrate.linux-amd64.tar.gz | tar xvz \
  && mv ./migrate.linux-amd64 /usr/local/bin/migrate && chmod +x /usr/local/bin/migrate \
  && curl -OL https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kubectl \
  && mv ./kubectl /usr/local/bin/kubectl && chmod +x /usr/local/bin/kubectl \
  && curl -L https://github.com/digitalocean/doctl/releases/download/v${DOCTL_VERSION}/doctl-${DOCTL_VERSION}-linux-amd64.tar.gz | tar xvz \
  && mv ./doctl /usr/local/bin/doctl && chmod +x /usr/local/bin/doctl

