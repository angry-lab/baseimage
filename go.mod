module gitlab.com/angry-lab/baseimage

go 1.15

require (
	github.com/cenkalti/backoff/v4 v4.1.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/lib/pq v1.8.0
	github.com/spf13/pflag v1.0.5
)
